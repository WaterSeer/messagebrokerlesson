﻿using System;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Brokers
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "crane.rmq.cloudamqp.com",
                VirtualHost = "jdiufojy",
                UserName = "jdiufojy",
                Password = "NQO0gkEeQr6LIuaz2jUwHmBkuV_j6iui"
            };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "EnjoyTheSilence",
                                            durable: false,
                                            exclusive: false,
                                            autoDelete: false,
                                            arguments: null
                                            );
                    var user = new Group { Name = "SilentProgrammers", SecretValue = "Ответ на главный вопрос жызни и всего такого: 42" };
                    string message = JsonConvert.SerializeObject(user);
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "",
                        routingKey: "EnjoyTheSilence",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine("[x] Sent {0} ", message);
                }
            }
        }
    }
    class Group
    {        public string Name { get; set; }
            public string SecretValue { get; set; }
            
    }
}
